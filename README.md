# MOSIP
Modular Open Source Identity Platform


Welcome to MOSIP!

The Modular Open Source Identity Platform (MOSIP) helps Governments and other user organizations implement a digital, foundational identity system in a cost effective way. Nations can use MOSIP freely to build their own identity systems. Being modular in its architecture, MOSIP provides flexibility to countries in how they implement and configure their systems, and helps avoid vendor lock-in.  It embraces the best practices of scalability, security and privacy and harnesses the power of open source.

----

We encourage you to first get an [overview](https://github.com/mosip-open/Documentation/wiki) of the MOSIP project and its [principles and architectural goals](https://github.com/mosip-open/Documentation/wiki/Architecture-Principles-&-Platform-Goals).
The [logical architecture](https://github.com/mosip-open/Documentation/wiki/Logical-Architecture) covers the key design aspects.
The [technology stack](https://github.com/mosip-open/Documentation/wiki/Technology-Stack) lists some of the key technologies used in MOSIP.

	
## To start using MOSIP
The ***seed contribution to MOSIP is still work in progress***, consider these code repo as an early preview for now. Today, we aim to give the community a sense of early direction with this preview. 

The issue list is open, but we will not act upon the issues till a formal release is behind us.

The Pre-Registration Client here is a reference implementation, written as a guide for implementors and help them build a UI that is relevant to their locale and configuration.

Pre-Registration Client is a web front end that relies on [Pre-Registration Services](https://github.com/mosip-open/mosip-prereg-services). The system designed to reach out to population at large and help ID seekers find and book a convenient appointment at a nearby registration center.

### Pre-Registration Client
Start with [functional requirement specification](https://github.com/mosip-open/Documentation/wiki/FRS-Pre-Registration) and follow on to the design of [Pre-Registration services](https://github.com/mosip-open/mosip-prereg-services) 

To get a complete running client and backend, follow the instructions in [Getting Started UI](Getting-started-ui.md) guide.


## More information
Visit [our website](http://www.mosip.io) for more information, and to contact us.

